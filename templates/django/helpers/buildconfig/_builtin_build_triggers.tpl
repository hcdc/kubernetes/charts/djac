
{{- define "django.builtinBuildTriggers" -}}
triggers:
  - imageChange: {}
    type: ImageChange
{{- if eq .Values.fromImage "python-latex:latest" }}
  - imageChange:
      from:
        kind: ImageStreamTag
        name: python-latex:latest
    type: ImageChange
{{- end }}
{{- end }}
