
{{- define "django.builtinVolumes" -}}
media:
  storage: "1Gi"
  keep: true
  readOnly: false
  mountPath: "/opt/app-root/src/media/public"
internal-media:
  storage: "15Gi"
  keep: true
  readOnly: false
  mountPath: "/opt/app-root/src/media/internal"
{{- end }}
