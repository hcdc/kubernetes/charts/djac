
{{- define "django.builtinCronJobs" -}}
minutely-jobs:
  command: [python, manage.py, runjobs, minutely]
  scheduling:
    concurrencyPolicy: "Forbid"
{{- end }}
